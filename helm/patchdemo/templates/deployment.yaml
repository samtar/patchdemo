apiVersion: v1
kind: PersistentVolume
metadata:
  name: {{ include "patchdemo.fullname" . }}-wiki-repos-pv
spec:
  storageClassName: manual
  capacity:
    storage: 15Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  hostPath:
    path: {{ .Values.refWikiRepos }}
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ include "patchdemo.fullname" . }}-wiki-repos-pvc
spec:
  volumeName: {{ include "patchdemo.fullname" . }}-wiki-repos-pv
  storageClassName: manual
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 15Gi
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ include "patchdemo.fullname" . }}-sessions
spec:
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ include "patchdemo.fullname" . }}
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
    limits:
      storage: 80Gi
  volumeMode: Filesystem

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "patchdemo.fullname" . }}-repos-config
data:
  repos.cfg: |
{{ $.Files.Get "files/repos.cfg" | indent 4 }}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "patchdemo.fullname" . }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      app: {{ include "patchdemo.fullname" . }}
  template:
    metadata:
      labels:
        app: {{ include "patchdemo.fullname" . }}
    spec:
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
          env:
            - name: DB_USER
              value: "{{ .Values.mariadb.auth.username }}"
            - name: DB_PASS
              value: "{{ .Values.mariadb.auth.password }}"
            - name: DB_HOST
              value: "{{ .Release.Name }}-{{ .Values.app.db.host }}"
            - name: DB_DATABASE
              value: "{{ .Values.mariadb.auth.database }}"
            - name: NEW_WIKI_WARNING
              value: "{{ .Values.app.newWikiWarning }}"
            - name: NOTIFICATION_BANNER
              value: "{{ .Values.app.notificationBanner }}"
            - name: PHABRICATOR_URL
              value: "{{ .Values.app.phabricatorUrl }}"
            - name: GERRIT_URL
              value: "{{ .Values.app.gerritUrl }}"
            - name: CATALYST_API_URL
              value: "{{ .Values.app.catalystApiUrl }}"
            - name: CATALYST_DOMAIN_NAME
              value: "{{ .Values.app.catalystDomainName }}"
            - name: CATALYST_API_TOKEN
              value: "{{ .Values.app.catalystApiToken }}"
            - name: STATUS_URL
              value: "{{ .Values.app.statusUrl }}"
            - name: REQUIRE_VERIFIED
              value: "{{ .Values.app.requireVerified }}"
            - name: OAUTH_URL
              value: "{{ .Values.app.oauthUrl }}"
            - name: OAUTH_CALLBACK
              value: "{{ .Values.app.oauthCallback }}"
            - name: OAUTH_CONSUMER_KEY
              value: "{{ .Values.app.oauthConsumerKey }}"
            - name: OAUTH_CONSUMER_SECRET
              value: "{{ .Values.app.oauthConsumerSecret }}"
            - name: ADMIN_USERS
              value: "{{ .Values.app.adminUsers }}"
            - name: CONDUIT_API_KEY
              value: "{{ .Values.app.conduitApiKey }}"
          volumeMounts:
            - name: wikis
              mountPath: /var/www/html/wikis
            - name: sessions
              mountPath: /var/lib/php/sessions
            - name: log-volume
              mountPath: /var/log/apache2
        - name: tail-access-log
          image: busybox
          command: ["sh", "-c", "tail -f /var/log/apache2/access.log"]
          volumeMounts:
            - name: log-volume
              mountPath: /var/log/apache2
        - name: tail-error-log
          image: busybox
          command: ["sh", "-c", "tail -f /var/log/apache2/error.log"]
          volumeMounts:
            - name: log-volume
              mountPath: /var/log/apache2
        - name: tail-other-vhosts-access-log
          image: busybox
          command: ["sh", "-c", "tail -f /var/log/apache2/other_vhosts_access.log"]
          volumeMounts:
            - name: log-volume
              mountPath: /var/log/apache2
      initContainers:
        - name: init-permissions
          image: busybox
          command: ["sh", "-c", "chown -R 33:33 /var/log/apache2"]
          volumeMounts:
            - name: log-volume
              mountPath: /var/log/apache2
        - name: clone-wiki-repos
          image: bitnami/git:latest
          command:
            - bash
            - -c
            - |
              set -eu -o pipefail

              GERRIT_URL="https://gerrit.wikimedia.org/r"

              cd /srv/repo-pool/wiki-repos
              if [ ! -d core/.git ]; then
                git clone --no-checkout $GERRIT_URL/mediawiki/core core
              fi
              while read -r source target; do
                if [ ! -d "$target/.git" ]; then
                  git clone --no-checkout "$GERRIT_URL/$source" "$target"
                fi
              done <../config/repos.cfg
          volumeMounts:
            - name: wiki-repos
              mountPath: /srv/repo-pool/wiki-repos
              readOnly: false
            - name: repos-config
              mountPath: /srv/repo-pool/config
      volumes:
        - name: wikis
          persistentVolumeClaim:
            claimName: {{ include "patchdemo.fullname" . }}
        - name: sessions
          persistentVolumeClaim:
            claimName: {{ include "patchdemo.fullname" . }}-sessions
        - name: wiki-repos
          persistentVolumeClaim:
            claimName: {{ include "patchdemo.fullname" . }}-wiki-repos-pvc
        - name: repos-config
          configMap:
            name: {{ include "patchdemo.fullname" . }}-repos-config
        - name: log-volume
